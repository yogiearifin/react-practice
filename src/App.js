import React from "react";
import Routes from "./routes/router";
import { Provider } from "react-redux";
import store from "./store";
import Header from "./components/Header-Footer/Header";
import Footer from "./components/Header-Footer/Footer";

function App() {
  return (
    <Provider store={store}>
      <Header />
      <Routes />
      <Footer />
    </Provider>
  );
}

export default App;
