import "bootstrap/dist/css/bootstrap.min.css";
import React from "react";
import { ModalBody } from "reactstrap";
import "./Ary.css";
import { Link } from "react-router-dom";

function Ary(props) {
  const { name, desc, img } = props;
  console.log(props);
  return (
    <>
      <ModalBody>
        <Link to="/Ary"> <h1>{name}</h1> </Link> 
        <p>{desc}</p>
        <img src={img} alt="pepe the frog" />
      </ModalBody>
    </>
  );
}

export default Ary;