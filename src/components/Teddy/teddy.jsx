import "bootstrap/dist/css/bootstrap.min.css";
import React from "react";
import { ModalBody } from "reactstrap";
import "./assets/teddy.png";
import { Link } from "react-router-dom";

function Teddy(props) {
    const { name, desc, img } = props;
    console.log(props);
    return (
        <>
            <ModalBody>
                <Link to="/Teddy"><h1>{name}</h1></Link>
                <p>{desc}</p>
                <img src={img} alt="I DONT KNOW" />
            </ModalBody>
        </>
    );
}

export default Teddy;