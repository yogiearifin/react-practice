import "bootstrap/dist/css/bootstrap.min.css";
import React from "react";
import { ModalBody } from "reactstrap";
import "./assets/zuhry.css";
import { Link } from "react-router-dom";


function Zuhry(props) {
  const { name, desc, img } = props;
  console.log(props);
  return (
    <>
      <ModalBody>
        <Link to="/zuhry"><h1>{name}</h1></Link>
        <p>{desc}</p>
        <img src={img} alt="dead inside tho" />
      </ModalBody>
    </>
  );
}

export default Zuhry;
