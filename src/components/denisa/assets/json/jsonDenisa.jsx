import React from "react";
import axios from "axios";
import "../../../../pages/denisa/assets/pageDenisa.scss"
import { Link } from "react-router-dom";
import {
  Row,
  Col,
  Card,
  Button,
} from 'reactstrap';

class JsonDenisa extends React.Component {
    state = {
        jsonData: []
    };

    componentDidMount() {
        axios
          .get("https://jsonplaceholder.typicode.com/posts")
          .then((res) => {
              this.setState({
                  jsonData: res.data,
              });
          })
          .catch((err) => {
              console.log(err);
          });
    }

          render() {
              console.log(this.state.jsonData);
              return (
                <> 
                {this.state.jsonData.map((data, index) => {
                    return (
                            <Row className="rowy">
                                <Col md="5">
                                    <Card className="scss-card">
                                        <div key={index}>
                                            <h3>{data.title}</h3>
                                            <p>{data.id}</p>
                                            <p>{data.body}</p>
                                            <Button className="butt"><Link to="/crudDenisa">Form!</Link></Button>
                                        </div>
                                    </Card>
                                </Col>
                            </Row>
                    );
                })}
                </>
              );
          }
    
}

export default JsonDenisa