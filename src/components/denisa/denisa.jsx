import "bootstrap/dist/css/bootstrap.min.css";
import React from "react";
import { ModalBody } from "reactstrap";
import "./assets/denisa.css";
import { Link } from "react-router-dom";


function Denisa(props) {
  const { name, desc, img } = props;
  console.log(props);
  return (
    <>
      <ModalBody>
        <Link to="/denisa"><h1>{name}</h1></Link>
        <p>{desc}</p>
        <img src={img} alt="menacing" />
      </ModalBody>
    </>
  );
}

export default Denisa;