import "bootstrap/dist/css/bootstrap.min.css";
import React from "react";
import { ModalBody } from "reactstrap";
import "./assets/denny.css";
import { Link } from "react-router-dom";

function Denny(props) {
  const { name, desc } = props;
  console.log(props);
  return (
    <>
      <ModalBody>
        <Link to="/denny">
          <h1 className="text-dark mx-auto">{name}</h1>
        </Link>
        <p>{desc}</p>
      </ModalBody>
    </>
  );
}

export default Denny;