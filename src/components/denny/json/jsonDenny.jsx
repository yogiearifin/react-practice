import React from "react";
import axios from "axios";
import { Container } from "reactstrap";
import '../assets/json.css'
import { Link } from "react-router-dom";

class JsonDenny extends React.Component {
  state = {
    jsonData: [],
  };

  componentDidMount() {
    axios
      .get("https://jsonplaceholder.typicode.com/posts")
      .then((res) => {
        this.setState({
          jsonData: res.data,
        });
      })
      .catch((err) => {
        console.log(err);
      });
  }
  render() {
    console.log(this.state.jsonData);
    return (
      <>
        <Container>
          <Link to="/denny">Go Back</Link>
          {this.state.jsonData.map((data, index) => {
            return (
                <div className="text-dark mt-5" key={index}>
                  <h1>{data.id}. {data.title}</h1>
                  <p className="text-justify">{data.body}</p>
                </div>
          );
        })}
        </Container>
      </>
    );
  }
}

export default JsonDenny;
