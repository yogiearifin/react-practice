import "bootstrap/dist/css/bootstrap.min.css";
import React from "react";
import { ModalBody, Spinner } from "reactstrap";
import "./assets/gilang.css";
import { Link } from "react-router-dom";

function Gilang(props) {
  const { name, desc, image } = props;
  return (
    <>
      <ModalBody className="gil">
      <Link to="/gilang"><h1><i>{name}</i></h1></Link>
        <br/>
        <Spinner type="grow" color="primary" />
        <Spinner type="grow" color="secondary" />
        <Spinner type="grow" color="success" />
        <Spinner type="grow" color="danger" />
        <Spinner type="grow" color="warning" />
        <Spinner type="grow" color="info" />
        <Spinner type="grow" color="light" />
        <Spinner type="grow" color="dark" />
        <p><i>{desc}</i></p>
        <img src={image} alt="logo" />
        
        
      </ModalBody>
      
    </>
  );
}

export default Gilang;