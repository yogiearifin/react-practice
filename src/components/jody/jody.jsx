import "bootstrap/dist/css/bootstrap.min.css";
import React from "react";
import { ModalBody } from "reactstrap";
import "./assets/jody.css";
import { Link } from "react-router-dom";


function Jody(props) {
  const { name, desc, img } = props;
  console.log(props);
  return (
    <>
      <ModalBody>
        <Link to="/jody"><h1>{name}</h1></Link>
        <p>{desc}</p>
        <img style={{width: "465px"}} src="https://drive.google.com/uc?export=view&id=1ghxOFmdOy7ex6wq9VAO3LfAUsd3XmJ1v" alt="menacing" />
      </ModalBody>
    </>
  );
}

export default Jody;
