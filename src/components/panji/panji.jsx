import "bootstrap/dist/css/bootstrap.min.css";
import React from "react";
import { ModalBody } from "reactstrap";

function Panji(props) {
  const { name, desc, img } = props;
  console.log(props);
  return (
    <>
      <ModalBody>
        <h1>{name}</h1>
        <p>{desc}</p>
        <img src={img} alt="blabla" />
      </ModalBody>
    </>
  );
}

export default Panji;
