import "bootstrap/dist/css/bootstrap.min.css";
// import React, { useState } from "react";
import { ModalBody } from "reactstrap";
import "./assets/sugianto.css";
import { Link } from "react-router-dom";

function Sugi(props) {
  const {name, desc, img} = props;
  return (
    <>
      <ModalBody>
        <Link to = './sugianto' ><h1>{name}</h1></Link>
        <p>{desc}</p>
        <img src={img} alt="hulk" />
      </ModalBody>
      
    </>
  );
}

export default Sugi;
