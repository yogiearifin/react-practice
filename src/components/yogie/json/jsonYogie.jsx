import React from "react";
import axios from "axios";
import { Link } from "react-router-dom";

class JsonYogie extends React.Component {
  state = {
    jsonData: [],
  };

  componentDidMount() {
    axios
      .get("https://jsonplaceholder.typicode.com/posts")
      .then((res) => {
        this.setState({
          jsonData: res.data,
        });
      })
      .catch((err) => {
        console.log(err);
      });
  }
  render() {
    console.log(this.state.jsonData);
    return (
      <>
        <Link to="/yogie-crud">
          <h1>CRUD</h1>
        </Link>
        {this.state.jsonData.map((data, idx) => {
          return (
            <div key={idx}>
              <h1>{data.title}</h1>
              <p>{data.id}</p>
              <p>{data.body}</p>
            </div>
          );
        })}
      </>
    );
  }
}

export default JsonYogie;
