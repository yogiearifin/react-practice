import "bootstrap/dist/css/bootstrap.min.css";
import React from "react";
import { ModalBody } from "reactstrap";
import "./assets/yogie.css";
import { Link } from "react-router-dom";

function Yogie(props) {
  const { name, desc, img } = props;
  console.log(props);
  return (
    <>
      <ModalBody>
        <Link to="/yogie">
          <h1>{name}</h1>
        </Link>
        <p>{desc}</p>
        <div>
          <div className="image-center">
            <img src={img} alt="menacing" className="image-comp" />
          </div>
        </div>
      </ModalBody>
    </>
  );
}

export default Yogie;
