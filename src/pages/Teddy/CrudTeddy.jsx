import React, { Component } from 'react';
import { Col, Row, Button, Form, FormGroup, Label, Input, Container } from 'reactstrap';
import axios from 'axios';

class CrudTeddy extends Component {
    state = {

        userData: {
            name: '',
            age: null,
            desc: ''
        },
        data: [],
        // Post
        inputUser: {
            id: null,
            title: '',
            body: '',
        }
    }

    // Bagian Onchange Input
    inputData = (e) => {
        // console.log(e.target.value);
        this.setState({
            userData: {
                ...this.state.userData,
                // Selector di bagian name / id pada input
                [e.target.id]: e.target.value
            }
        })
    }

    // Bagian Onchange Post 
    inputFromUser = (e) => {
        // console.log(e.target.value);
        this.setState({
            inputUser: {
                ...this.state.inputUser,
                // Selector di bagian name / id pada input
                [e.target.id]: e.target.value
            }
        })
    }

    // Button Post
    postFromUser = () => {
        const allInputData = this.state.inputUser;
        axios.post('https://jsonplaceholder.typicode.com/posts/', { allInputData })
            .then((resultData) => {
                console.log(resultData)
            })
    }

    // Di bagian Onclick
    outputData = () => {
        let newInputData = [...this.state.data]
        newInputData.push({
            name: this.state.userData.name,
            age: this.state.userData.age,
            desc: this.state.userData.desc
        })
        this.setState({
            data: newInputData
        })
    }

    render() {
        console.log(this.state.userData)
        console.log(this.state.inputUser)
        return (
            <div>
                <h1>Welcome To My App</h1>
                <Container>
                    <Form>
                        <Row form>
                            <Col md={6}>
                                <FormGroup>
                                    <Label for="exampleEmail">Name</Label>
                                    <Input type="text" name="name" id="name" placeholder="with a placeholder" onChange={this.inputData} value={this.state.userData.name} />
                                </FormGroup>
                            </Col>
                            <Col md={6}>
                                <FormGroup>
                                    <Label for="examplePassword">Age</Label>
                                    <Input type="text" name="age" id="age" placeholder="Age placeholder" onChange={this.inputData} value={this.state.userData.age} />
                                </FormGroup>
                            </Col>
                        </Row>
                        <FormGroup>
                            <Label for="exampleAddress">Desc</Label>
                            <Input type="text" name="desc" id="desc" placeholder="1234 Main St" onChange={this.inputData} value={this.state.userData.desc} />
                        </FormGroup>
                        <Button onClick={this.outputData}>Sign in</Button>
                    </Form>
                </Container>

                <Container>
                    {
                        this.state.data.map((result, i) => {
                            return (
                                <div key={result.name + '_' + i}>
                                    <p>{result.name}</p>
                                    <p>{result.age}</p>
                                    <p>{result.desc}</p>
                                </div>
                            )
                        })
                    }
                    <br /><br /><br /><br /><br />



                    <h1>Post</h1>
                    <Form>
                        <Row form>
                            <Col md={6}>
                                <FormGroup>
                                    <Label for="exampleEmail">ID</Label>
                                    <Input type="number" name="id" id="id" placeholder="Input Your ID" onChange={this.inputFromUser} value={this.state.inputUser.id} />
                                </FormGroup>
                            </Col>
                            <Col md={6}>
                                <FormGroup>
                                    <Label for="examplePassword">Title</Label>
                                    <Input type="text" name="title" id="title" placeholder="Age placeholder" onChange={this.inputFromUser} value={this.state.inputUser.title} />
                                </FormGroup>
                            </Col>
                        </Row>
                        <FormGroup>
                            <Label for="exampleAddress">Body</Label>
                            <Input type="text" name="body" id="body" placeholder="Body" onChange={this.inputFromUser} value={this.state.inputUser.body} />
                        </FormGroup>
                        <Button onClick={this.postFromUser}>Post</Button>
                    </Form>
                    <br /><br /><br /><br /><br /><br /><br /><br /><br /><br />
                </Container>
            </div>
        )
    }
}


export default CrudTeddy;