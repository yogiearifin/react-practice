import React, { useState, useEffect } from 'react';

const HooksTeddy = () => {
    const [number, countNumber] = useState(0);

    let plusNumber = () => {
        return countNumber(number + 1);
    }
    let minusNumber = () => {
        return countNumber(number - 1)
    }

    useEffect(() => {
        console.log("STATES");
    }, [number])

    return (
        <div>
            <h1>Counting Number</h1>
            <button onClick={minusNumber}>-</button>
            <span>{number}</span>
            <button onClick={plusNumber}>+</button>
        </div>
    )
}

export default HooksTeddy;