import React, { useState } from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import TeddyJson from './TeddyJson';
import Images from '../Teddy/assets/image.jpg';
import { Link } from 'react-router-dom';
import {
    Collapse,
    Navbar,
    NavbarToggler,
    NavbarBrand,
    Nav,
    NavItem,
    NavLink,
    UncontrolledDropdown,
    DropdownToggle,
    DropdownMenu,
    DropdownItem,
    NavbarText,
    Col,
    ModalBody,
} from 'reactstrap';
import HooksTeddy from './HooksTeddy';

const PageTeddy = () => {
    const [isOpen, setIsOpen] = useState(false);

    const toggle = () => setIsOpen(!isOpen);
    return (
        <div>
            <Navbar color="light" light expand="md">
                <NavbarBrand href="/">reactstrap</NavbarBrand>
                <NavbarToggler onClick={toggle} />
                <Collapse isOpen={isOpen} navbar>
                    <Nav className="mr-auto" navbar>
                        <NavItem>
                            <NavLink href="/components/">Components</NavLink>
                        </NavItem>
                        <NavItem>
                            <NavLink href="/HooksTeddy/">Hooks Counting Number</NavLink>
                        </NavItem>
                        <NavItem>
                            <NavLink href="/HooksTeddy/">Hooks Counting Number</NavLink>
                        </NavItem>
                        <NavItem>
                            <NavLink href="https://github.com/reactstrap/reactstrap">GitHub</NavLink>
                        </NavItem>
                        <UncontrolledDropdown nav inNavbar>
                            <DropdownToggle nav caret>
                                Options
              </DropdownToggle>
                            <DropdownMenu right>
                                <DropdownItem>
                                    Option 1
                </DropdownItem>
                                <DropdownItem>
                                    Option 2
                </DropdownItem>
                                <DropdownItem divider />
                                <DropdownItem>
                                    Reset
                </DropdownItem>
                            </DropdownMenu>
                        </UncontrolledDropdown>
                    </Nav>
                    <NavbarText>Simple Text</NavbarText>
                </Collapse>
            </Navbar>
            <Col md="10"><ModalBody className="">
                <Link to="./TeddyJson"><h1>Teddy Ferdian Abrar Amrullah</h1></Link>
                <img src={Images} alt="WKWKWK" />
                <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Laboriosam facere dolores quaerat delectus! Totam maxime molestias cum, esse quis labore voluptatem neque saepe fugiat assumenda. Natus quae, officiis, nesciunt illo, consequatur itaque vitae reprehenderit alias in tempore vero sequi rerum dicta debitis quia architecto magnam eos tempora repudiandae! Autem ab maiores aspernatur nulla possimus, aliquam esse delectus tempora dignissimos corrupti illum. Autem magnam architecto minus ex beatae? Odit quod eius dicta. Doloremque enim sequi repudiandae incidunt. Consectetur possimus harum ab ut ducimus maiores quis libero quod dolorum nobis, excepturi quibusdam quo provident iure labore illum explicabo aut fugit! Incidunt reiciendis eos quas magnam, nam necessitatibus quae. Eaque, nihil cumque rem laudantium ducimus id nemo natus. Repellendus dolorem tempora vero corporis! Beatae, dolore consequuntur error fugit ipsam nisi, reiciendis aut fugiat, accusantium labore quas sequi molestiae. Minus soluta itaque tempore exercitationem aliquid repellendus possimus eveniet animi blanditiis facilis, similique ipsam delectus minima nemo deleniti, eligendi perferendis? Sed provident possimus commodi voluptatibus suscipit fuga, deserunt omnis, atque, iste ipsa quod! Quibusdam a facilis, sed ipsum dolor sit sunt id recusandae neque at voluptatem minus repellat numquam nam molestias consequuntur ipsam dignissimos accusamus saepe tempora, architecto, quasi exercitationem! Vitae architecto nisi commodi at exercitationem. Commodi voluptatibus sint beatae rem. Deserunt nostrum nobis corrupti itaque, totam obcaecati sed adipisci, iure incidunt iste molestias neque et commodi ad odio minima reiciendis perferendis fuga, ratione doloremque fugiat! At molestiae ipsum voluptatem aperiam nihil id numquam, rerum a accusamus beatae deleniti minus temporibus alias natus aliquam commodi et ratione eveniet recusandae. Perspiciatis adipisci praesentium sunt, assumenda ex dolorem voluptatem corporis saepe maxime temporibus eligendi! Quisquam, provident? Rerum quidem assumenda, ipsa deserunt ducimus dolorem dolor quas ullam quos libero cupiditate neque doloribus quisquam nihil quae omnis a ut deleniti doloremque quis aliquid dicta veritatis quibusdam enim? Fuga, vero iure? Odit sunt eum dolorem necessitatibus rem dolores, pariatur molestias harum quidem fugit inventore beatae eos quaerat eveniet cumque a aut voluptates voluptas quibusdam, dignissimos voluptatibus molestiae vitae quae? Eum, fugiat perferendis unde in exercitationem quia voluptatum velit perspiciatis, quibusdam labore nulla. Nemo doloribus aspernatur illum totam velit eum temporibus nesciunt, voluptas quibusdam odit autem quaerat cupiditate dicta repellendus ab ut tempore impedit neque nisi nam nostrum, eveniet facilis quos repellat? Possimus ducimus modi natus optio, quidem et repellat odit ipsam nihil sunt voluptates facilis aspernatur accusamus animi quia, laudantium beatae recusandae ipsum expedita quod commodi. Velit repellendus aperiam labore quasi soluta minus est numquam perferendis similique eaque asperiores esse, facere, repudiandae necessitatibus? Neque dolores eos fuga expedita est quam nesciunt magni fugiat laudantium magnam velit sed enim necessitatibus esse unde illum, incidunt doloribus soluta totam inventore pariatur repudiandae ipsum tenetur quibusdam? Perferendis aspernatur ipsa nemo possimus et fugiat quasi inventore, odit omnis voluptatibus est, dignissimos, ea id sint. Eaque error quia deleniti ipsum, ea iure? Repudiandae, laborum nostrum? Natus consequuntur minima fuga, omnis corrupti culpa quidem distinctio, atque neque iusto enim vitae laborum eveniet recusandae rem eaque repellat sint architecto nesciunt incidunt voluptate dolore ullam minus iure. Ab, in?</p>
            </ModalBody></Col>
        </div>
    );
};


export default PageTeddy;