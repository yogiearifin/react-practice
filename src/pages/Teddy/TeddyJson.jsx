import React, { Component } from 'react';
import axios from 'axios';
import { Link } from 'react-router-dom';


class TeddyJson extends Component {
    constructor(props) {
        super(props)
        this.state = {
            data: []
        }
    }
    componentDidMount() {
        axios.get('https://jsonplaceholder.typicode.com/posts')
            .then((result) => {
                console.log(result);
                this.setState({
                    //                Object Yang mau di ambil
                    data: result.data
                })
            })
    }

    render() {
        return (
            <div>
                <Link to='/CrudTeddy'><h1>My CRUD</h1></Link>
                <h1>JSON FILE</h1>
                {
                    this.state.data.map(data => {
                        return (
                            <div>
                                <p>Id : {data.id} </p>
                                <p>Name : {data.title}</p>
                                <p>Body : {data.body}</p>
                            </div>
                        )
                    })
                }
            </div>
        )
    }
}


export default TeddyJson;