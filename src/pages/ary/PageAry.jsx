import "bootstrap/dist/css/bootstrap.min.css";
import React from "react";
import { ModalBody, Container, Row, Col, Navbar, NavbarBrand, Nav, NavLink, Button  } from "reactstrap";
import "./assets/pageary.css";
import profile from "./assets/squidward.png";
import image from "./assets/tenor.gif";

const PageAry = () => {
    return (
        <div>
            <Navbar color="dark" light expand="md">
        <Container fluid>
          <NavbarBrand href="/Ary"><h1 className="text-light">Ary Tantranesia</h1></NavbarBrand>
          <Nav className="mr-auto">
            <NavLink href="#" className="text-light">Concert</NavLink>
            <NavLink href="#" className="text-light">About Soft Boi</NavLink>
            <NavLink href="#" className="text-light">Krusty Krab's</NavLink>
          </Nav> 
          <NavLink href="/" className="text-light">Go Back</NavLink>
        </Container>      
      </Navbar>

      <Container fluid className="mt-5">
        <Row>
          <Col md="3" className="text-center border-right border-muted">
            <img className="profile" src={profile} alt="medium"/>
            <h4 className="text-dark mt-3">Squidward</h4>
            <p className="text-muted">Handsome boi. He is a very cynical, selfish, stick-in-the-mud individual. He works as the cashier at the Krusty Krab, a job he hates. Squidward is frequently annoyed by SpongeBob's loud and cheerful behavior, but he sometimes sticks up for SpongeBob and sees him as a friend on occasion.</p>
            <Button color="light"><i className="fab fa-instagram"></i></Button>
            </Col>
          <Col md="9">
            <img className="image" src={image} alt="medium"/>
            <p className="text-dark text-center mt-3">Forever alone and sad boi</p>
            </Col>
        </Row>
      </Container>


        </div>
    )

}

export default PageAry