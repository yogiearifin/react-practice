import React, {useState} from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import { 
  Button,
  Container,
  Collapse,
  Navbar,
  NavbarToggler,
  NavbarBrand,
  Nav,
  NavItem,
  NavLink,
  UncontrolledDropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem,
  InputGroup, InputGroupAddon, InputGroupText, Input
} from "reactstrap";
// import { FaSearch } from "react-icons/fa";
import "./assets/bromo.css";
// import { Link } from "react-router-dom";
import pic1 from "./assets/bromo.jpeg"
import pic2 from "./assets/bromo.jpeg"


const Bromo = () => {
  const [isOpen, setIsOpen] = useState(false);
  const toggle = () => setIsOpen(!isOpen);

  const navbar = {
    height: '100px',
    position: 'fixed',
    top: '0',
    width: '100%',
    zIndex: '999'
  }
  const inputGroup = {
    width: '300px',
    marginRight: '10px'
  };
  const webTitle = {
    fontSize: '45px',
    fontWeight: 'bold',
    marginLeft: '20px',
    fontFamily: 'monospace'
  }


  const fontSmall = {
    fontFamily:'monospace', 
    fontWeight:'bold', 
    fontSize:'16px'
  };
  const fontBig = {
    fontFamily:'monospace', 
    fontWeight:'bold', 
    fontSize:'50px'
  };
  const wrapper = {
    display: 'flex',
    marginTop: '100px',
    fontFamily: 'monospace'
  }


  return (
    <Container>
       <Navbar color="light" light expand="md" style={navbar}>
        <NavbarBrand href="#" style={webTitle}>Goks</NavbarBrand>
        <NavbarToggler onClick={toggle} />
        <Collapse isOpen={isOpen} navbar>
          <Nav className="mr-auto" navbar>
          <NavItem>
              <NavLink href="#">Unlimited Following</NavLink>
            </NavItem>
            <NavItem>
              <NavLink href="#">Tentang</NavLink>
            </NavItem>
            <NavItem>
              <NavLink href="#">Pengen Punya Followers :(</NavLink>
            </NavItem>
          </Nav>
          <InputGroup style={inputGroup}>
            <Input />
            <InputGroupAddon addonType="append">
              <Button color="warning"><i class="fas fa-search"></i></Button>
            </InputGroupAddon>
          </InputGroup>
          <Button href='/' color="warning">BALIK !</Button>
        </Collapse>
      </Navbar>
      <Container style={wrapper}>
        <div className='sidebarLeft'>
          <img src={pic1} alt=''/>
          <h6>Tentang</h6>
          <h5>Bromo Yudo Wibowo</h5>
          <p className='fe'>BUKAN KETUA KELAS</p>
          <p style={fontSmall}>Jadi ketua kelas itu ga enak lo gessss...</p>
        </div>
        <div className='sidebarRight'>
          <h6 style={fontSmall}>Published by G A</h6>
          <h1 style={fontBig}>Tips And Trick Menggulingkan Rezim Ketua Kelas Terdahulu</h1>
          <img src={pic2}/>
          <p>Zombie ipsum reversus ab viral inferno, nam rick grimes malum cerebro. De carne lumbering animata corpora quaeritis. Summus brains sit​​, morbo vel maleficia? De apocalypsi gorger omero undead survivor dictum mauris. Hi mindless mortuis soulless creaturas, imo evil stalking monstra adventus resi dentevil vultus comedat cerebella viventium. Qui animated corpse, cricket bat max brucks terribilem incessu zomby. The voodoo sacerdos flesh eater, suscitat mortuos comedere carnem virus. Zonbi tattered for solum oculi eorum defunctis go lum cerebro. Nescio brains an Undead zombies. Sicut malus putrid voodoo horror. Nigh tofth eliv ingdead.</p>
          <p>Zombie ipsum reversus ab viral inferno, nam rick grimes malum cerebro. De carne lumbering animata corpora quaeritis. Summus brains sit​​, morbo vel maleficia? De apocalypsi gorger omero undead survivor dictum mauris. Hi mindless mortuis soulless creaturas, imo evil stalking monstra adventus resi dentevil vultus comedat cerebella viventium. Qui animated corpse, cricket bat max brucks terribilem incessu zomby. The voodoo sacerdos flesh eater, suscitat mortuos comedere carnem virus. Zonbi tattered for solum oculi eorum defunctis go lum cerebro. Nescio brains an Undead zombies. Sicut malus putrid voodoo horror. Nigh tofth eliv ingdead. Zombie ipsum reversus ab viral inferno, nam rick grimes malum cerebro. De carne lumbering animata corpora quaeritis. Summus brains sit​​, morbo vel maleficia? De apocalypsi gorger omero undead survivor dictum mauris. Hi mindless mortuis soulless creaturas, imo evil stalking monstra adventus resi dentevil vultus comedat cerebella viventium. Qui animated corpse, cricket bat max brucks terribilem incessu zomby. The voodoo sacerdos flesh eater, suscitat mortuos comedere carnem virus. Zonbi tattered for solum oculi eorum defunctis go lum cerebro. Nescio brains an Undead zombies. Sicut malus putrid voodoo horror. Nigh tofth eliv ingdead.</p>
        </div>
      </Container>
    </Container>
         
    
    
  )
};

export default Bromo;
