import React from 'react';
import "../../pages/denisa/crudDenisa.scss"
import axios from "axios";
import {
    Col,
    Row,
    Button,
    Form,
    FormGroup,
    Label,
    Input,
    Container,
} from 'reactstrap';

class CrudDenisa extends React.Component {
  state = {
    data: {
      name: "",
      age: "null",
      desc: "",
    },

  submit: [],
    post: {
      title: "",
      id: "null",
      body: "",
    },
  };

  onType = (x) => {
    console.log(x.target.value);
    this.setState({
      data: {
        ...this.state.data,
        [x.target.id]: x.target.value,
      },
    });
  };

  onInput = (x) => {
    console.log(x.target.value);
    this.setState({
      post: {
        ...this.state.post,
        [x.target.id]: x.target.value,
      },
    });
  };

  onSend = () => {
    let allData = [...this.state.submit];
    allData.push({
      name: this.state.data.name,
      age: this.state.data.age,
      desc: this.state.data.desc,
    });
    this.setState({
      submit: allData,
    });
  };

  onSub = () => {
    const data = this.state.post
    axios.post("https://jsonplaceholder.typicode.com/posts", 
      { data }).then((res) => {
      console.log(res)
    })
  }

  render() {
    console.log("data", this.state.data);
    console.log("submit", this.state.submit);
    return (
      <div className="div-container">
      <Container>
        <Form>
          <Row>
            <h3>Please enter your data below!</h3>
          </Row>
          <Row form>
            <Col md={7}>
              <FormGroup>
                <Label for="name">Name</Label>
                <Input
                type="text"
                name="name"
                id="name"
                placeholder="insert your name here"
                onChange={this.onType} />
              </FormGroup>
            </Col>
            <Col md={3}>
              <FormGroup>
                <Label for="age">Age</Label>
                <Input
                type="number"
                name="age"
                id="age"
                placeholder="insert your age here"
                onChange={this.onType} />
              </FormGroup>
            </Col>
          </Row>
          <Row>
            <Col md={10}>
              <FormGroup>
                <Label for="desc">Description</Label>
                <Input
                type="text"
                name="desc"
                id="desc"
                placeholder="insert your description here"
                onChange={this.onType} />
              </FormGroup>
            </Col>
          </Row>
          <Row>
            <Button className="buttony" onClick={this.onSend}>Submit</Button>
          </Row>
        </Form>
        {this.state.submit.map((data, index) => {
          return (
            <div key={index}>
              <p>{data.name}</p>
              <p>{data.age}</p>
              <p>{data.desc}</p>
            </div>
          );
        })}
        <br/>
        <br/>
        <Form>
          <Row>
            <h3>Post Submit</h3>
          </Row>
          <Row form>
            <Col md={7}>
              <FormGroup>
                <Label for="name">Name</Label>
                <Input
                type="text"
                name="title"
                id="title"
                placeholder="insert your name here"
                onChange={this.onInput} />
              </FormGroup>
            </Col>
            <Col md={3}>
              <FormGroup>
                <Label for="age">Age</Label>
                <Input
                type="number"
                name="id"
                id="id"
                placeholder="insert your age here"
                onChange={this.onInput} />
              </FormGroup>
            </Col>
          </Row>
          <Row>
            <Col md={10}>
              <FormGroup>
                <Label for="desc">Description</Label>
                <Input
                type="text"
                name="body"
                id="body"
                placeholder="insert your description here"
                onChange={this.onInput} />
              </FormGroup>
            </Col>
          </Row>
          <Row>
            <Button className="buttony" onClick={this.onSub}>Submit</Button>
          </Row>
        </Form>
      </Container>
      </div>
    )
  }
}

export default CrudDenisa