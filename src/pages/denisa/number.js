import React, { useState, useEffect } from "react";
import "../denisa/assets/number.scss"
import { Row } from "reactstrap"

function NumbOpt() {
  const [num, setNum] = useState(0);

  const handleClickMinus = () => {
    setNum(num - 1);
  };

  const handleClickPlus = () => {
    setNum(num + 1);
  };

  useEffect(() => {
    console.log("numb")
  }, [num]);

  return (
    <div className="operator">
      <h1>Number Operator</h1>
      <h3>{num}</h3>
      <button className="bttn1" onClick={handleClickMinus}>-</button>
      <button className="bttn2" onClick={handleClickPlus}>+</button>
    </div>
  );
}

export default NumbOpt;
