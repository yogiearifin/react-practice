import React from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import "./assets/PageDenny.css";
// import profile from "./assets/kimyongji.jpg";
// import image from './assets/kim-young-ji-nm.jpg';
import { Container, Row, Col, Navbar, NavbarBrand, Nav, NavLink, Button } from 'reactstrap';

const PageDenny = () => {
  return (
    <div>
      <Navbar color="dark" light expand="md">
        <Container fluid>
          <NavbarBrand href="/denny"><h1 className="text-light">Denny Kurniawan</h1></NavbarBrand>
          <Nav className="mr-auto">
            <NavLink href="#" className="text-light">Followers</NavLink>
            <NavLink href="#" className="text-light">About</NavLink>
            <NavLink href="#" className="text-light">Following</NavLink>
            <NavLink href="/denny-json" className="text-light">JSON</NavLink>
          </Nav>
          <NavLink href="/" className="text-light">Go Back</NavLink>
        </Container>
      </Navbar>

      <Container fluid className="mt-5">
        <Row>
          <Col md="3" className="text-center border-right border-muted">
            {/* <img className="profile" src={profile} alt="medium" /> */}
            <h4 className="text-dark mt-3">Kim Yong Ji (김용지)</h4>
            <p className="text-muted">My Favorite &hearts;</p>
            <Button color="light"><i className="fab fa-instagram"></i></Button>
          </Col>
          <Col md="9">
            {/* <img className="image" src={image} alt="medium" /> */}
            <p className="text-dark text-center mt-3">Isn't she pretty? &hearts;</p>
          </Col>
        </Row>
      </Container>
    </div>

  );
};

export default PageDenny;
