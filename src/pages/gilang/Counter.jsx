import React, { useState, useEffect } from "react";
import "./assets/Counters.sass";

function CounterGilang() {
  const [num, setNum] = useState(0);

  const handleClickMinus = () => {
    setNum(num - 1);
  };

  const handleClickPlus = () => {
    setNum(num + 1);
  };

  useEffect(() => {
    console.log("STATES");
    console.log(num);
  }, [num]);

  return (
    <div className="inOut">
      <h1 className="h1-gil">{num}</h1>
      <button onClick={handleClickMinus}>Minus!</button>
      <button onClick={handleClickPlus}>Plus!</button>
    </div>
  );
}

export default CounterGilang;
