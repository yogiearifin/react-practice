import React, { useState, useEffect } from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import "./assets/data.sass";
import axios from "axios";

const Data = () => {
  const [states, setStates] = useState([]);
  const getData = () => {
    axios.get("https://jsonplaceholder.typicode.com/posts").then((res) => {
      setStates(res.data);
    });
  };

  useEffect(() => {
    getData();
  }, []);


  return (
    <>
      {states.map((data, idx) => {
        return (
          <div className="Container" key={idx}>
            <div className="Inherit">
              <h1>Title: {data.title}</h1>
              <p>ID: {data.id} </p>
              <p>Body: {data.body}</p>
              <br />
              <br />
            </div>
          </div>
        );
      })}
    </>
  );
};

export default Data;
