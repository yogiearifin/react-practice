import React, { useState } from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import {
  Button,
  Container,
  Collapse,
  Navbar,
  NavbarToggler,
  NavbarBrand,
  Nav,
  NavItem,
  NavLink,
  UncontrolledDropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem,
  InputGroup,
  InputGroupAddon,
  Input,
} from "reactstrap";
import "./assets/PageGilang.sass";
import pic1 from "./assets/p1.png";
import pic2 from "./assets/p2.png";
import { Link } from "react-router-dom";

const PageGilang = () => {
  const [isOpen, setIsOpen] = useState(false);
  const toggle = () => setIsOpen(!isOpen);

  const navbar1 = {
    height: "100px",
    position: "fixed",
    top: "0",
    left: 0,
    width: "80%",
    zIndex: "999",
  };
  const inputGroup = {
    width: "300px",
    marginRight: "10px",
  };
  const webTitle = {
    fontSize: "45px",
    fontWeight: "bold",
    marginLeft: "20px",
    fontFamily: "monospace",
  };
  const fontSmall = {
    fontFamily: "monospace",
    fontWeight: "bold",
    fontSize: "16px",
  };
  const fontBig = {
    fontFamily: "monospace",
    fontWeight: "bold",
    fontSize: "50px",
  };
  const wrapper = {
    display: "flex",
    marginTop: "100px",
    fontFamily: "monospace",
  };

  return (
    <Container>
      <Navbar color="light" light expand="sm" style={navbar1}>
        <NavbarBrand href="https://gitlab.com/gilangadam" style={webTitle}>
          UHUY
        </NavbarBrand>
        <NavbarToggler onClick={toggle} />
        <Collapse isOpen={isOpen} navbar>
          <Nav className="mr-auto" navbar>
            <NavItem>
              <NavLink href="https://gitlab.com/gilangadam" >
              -99 Follower(s)?
              </NavLink>
            </NavItem>
            <NavItem>
              <NavLink href="https://gitlab.com/gilangadam">
                About
              </NavLink>
            </NavItem>
            <NavItem>
              <NavLink href="https://gitlab.com/gilangadam">
              -99 Following(s)?
              </NavLink>
            </NavItem>
            <UncontrolledDropdown nav inNavbar>
              <DropdownToggle nav caret>
                TASK
              </DropdownToggle>
              <DropdownMenu right>
                <DropdownItem>
                  <Link to="/data-gilang">Axios Data Fetching</Link>
                </DropdownItem>
                <DropdownItem>
                  <Link to="/crud-gilang">Simple CRUD</Link>
                </DropdownItem>
                <DropdownItem>
                  <Link to="/crud-axios-gilang">CRUD Post Axios</Link>
                </DropdownItem>
                <DropdownItem>
                  <Link to="/counter-gil">useState Counter</Link>
                </DropdownItem>
                <DropdownItem>
                  <Link to="/redux-simple">Simple Redux Hooks</Link>
                </DropdownItem>
                <DropdownItem>Option 3</DropdownItem>
                <DropdownItem divider />
                <DropdownItem>Reset</DropdownItem>
              </DropdownMenu>
            </UncontrolledDropdown>
          </Nav>
          <InputGroup style={inputGroup}>
            <Input />
            <InputGroupAddon addonType="append">
              <Button outline color="secondary">
                <i class="fas fa-search"></i>
              </Button>
            </InputGroupAddon>
          </InputGroup>
          <Button href="/" outline color="secondary">
            Back!
          </Button>
        </Collapse>
      </Navbar>
      <Container className="body-gil" style={wrapper}>
        <div className="sidebarLeft-gil">
          <img src={pic1} className="img-fluid mb-2 mt-2" alt="" />
          <h6>About</h6>
          <h5>Gilang Adam</h5>
          <p>Front-end Developer</p>
          <p style={fontSmall}>
            Zombie ipsum reversus ab viral inferno, nam rick grimes malum
            cerebro. De carne lumbering animata corpora quaeritis. Summus brains
            sit​​, morbo vel maleficia? De apocalypsi gorger omero undead
            survivor dictum mauris. Hi mindless mortuis soulless creaturas, imo
            evil stalking monstra adventus resi dentevil vultus comedat
            cerebella viventium.{" "}
          </p>
        </div>
        <div className="sidebarRight-gil">
          <h6 style={fontSmall}>Published by G A</h6>
          <h1 style={fontBig}>
            Bahagianya keluarga ini, kompak dan selalu mirip!
          </h1>
          <img className="img-fluid mb-2 mt-2" src={pic2} alt="" />
          <p>
            Zombie ipsum reversus ab viral inferno, nam rick grimes malum
            cerebro. De carne lumbering animata corpora quaeritis. Summus brains
            sit​​, morbo vel maleficia? De apocalypsi gorger omero undead
            survivor dictum mauris. Hi mindless mortuis soulless creaturas, imo
            evil stalking monstra adventus resi dentevil vultus comedat
            cerebella viventium. Qui animated corpse, cricket bat max brucks
            terribilem incessu zomby. The voodoo sacerdos flesh eater, suscitat
            mortuos comedere carnem virus. Zonbi tattered for solum oculi eorum
            defunctis go lum cerebro. Nescio brains an Undead zombies. Sicut
            malus putrid voodoo horror. Nigh tofth eliv ingdead.
          </p>
          <p>
            Zombie ipsum reversus ab viral inferno, nam rick grimes malum
            cerebro. De carne lumbering animata corpora quaeritis. Summus brains
            sit​​, morbo vel maleficia? De apocalypsi gorger omero undead
            survivor dictum mauris. Hi mindless mortuis soulless creaturas, imo
            evil stalking monstra adventus resi dentevil vultus comedat
            cerebella viventium. Qui animated corpse, cricket bat max brucks
            terribilem incessu zomby. The voodoo sacerdos flesh eater, suscitat
            mortuos comedere carnem virus. Zonbi tattered for solum oculi eorum
            defunctis go lum cerebro. Nescio brains an Undead zombies. Sicut
            malus putrid voodoo horror. Nigh tofth eliv ingdead. Zombie ipsum
            reversus ab viral inferno, nam rick grimes malum cerebro. De carne
            lumbering animata corpora quaeritis. Summus brains sit​​, morbo vel
            maleficia? De apocalypsi gorger omero undead survivor dictum mauris.
            Hi mindless mortuis soulless creaturas, imo evil stalking monstra
            adventus resi dentevil vultus comedat cerebella viventium. Qui
            animated corpse, cricket bat max brucks terribilem incessu zomby.
            The voodoo sacerdos flesh eater, suscitat mortuos comedere carnem
            virus. Zonbi tattered for solum oculi eorum defunctis go lum
            cerebro. Nescio brains an Undead zombies. Sicut malus putrid voodoo
            horror. Nigh tofth eliv ingdead.
          </p>
        </div>
      </Container>
    </Container>
  );
};

export default PageGilang;
