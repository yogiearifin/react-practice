import React, { Component } from "react";
import {
  Container,
  Button,
  Col,
  FormGroup,
  Input,
  Label,
  Form,
} from "reactstrap";
import axios from "axios";

class CrudAxiosGilang extends Component {
  state = {
    appData: {
      id: null,
      title: "",
      body: "",
    },
    submit: [],
  };

  onType = (e) => {
    this.setState({
      submit: {
        ...this.state.submit,
        [e.target.name]: e.target.value,
      },
    });
  };

  onSend = () => {
    const data = this.state.submit;
    axios
      .post("https://jsonplaceholder.typicode.com/posts", { data })
      .then((res) => {
        console.log(res);
      });
  };

  render() {
    return (
      <>
        <h1 className="text-center mb-5 mt-5">
          Simple CRUD App Using Post Axios
        </h1>
        <Container>
          <Form>
            <FormGroup row>
              <Label for="name" md={2}>
                ID
              </Label>
              <Col md={10}>
                <Input
                  type="number"
                  name="id"
                  id="id"
                  placeholder="ID"
                  onChange={this.onType}
                />
              </Col>
            </FormGroup>

            <FormGroup row>
              <Label for="age" md={2}>
                Title
              </Label>
              <Col md={10}>
                <Input
                  type="text"
                  name="title"
                  id="title"
                  placeholder="TITLE"
                  onChange={this.onType}
                />
              </Col>
            </FormGroup>

            <FormGroup row>
              <Label for="description" md={2}>
                Body
              </Label>
              <Col md={10}>
                <Input
                  type="textarea"
                  name="body"
                  id="body"
                  placeholder="BODY"
                  onChange={this.onType}
                />
              </Col>
            </FormGroup>
            <Button
              onClick={this.onSend}
              outline
              color="secondary"
              size="md"
              block
            >
              Submit
            </Button>
          </Form>
          <br />
        </Container>
      </>
    );
  }
}

export default CrudAxiosGilang;
