import React, { Component } from "react";
import {
  Container,
  Button,
  Col,
  FormGroup,
  Input,
  Label,
  Form,
} from "reactstrap";

class CrudGilang extends Component {
  state = {
    appData: {
      name: "",
      age: null,
      desc: "",
    },
    submit: [],
  };

  onType = (e) => {
    this.setState({
      appData: {
        ...this.state.appData,
        [e.target.name]: e.target.value,
      },
    });
  };

  onSend = () => {
    let allData = [...this.state.submit];
    allData.push({
      name: this.state.appData.name,
      age: this.state.appData.age,
      desc: this.state.appData.desc,
    });
    this.setState({
      submit: allData,
    });
  };

  render() {
    // const { submit } = this.state;
    return (
      <>
        <h1 className="text-center mb-5 mt-5">Simple CRUD App</h1>
        <Container>
          <Form>
            <FormGroup row>
              <Label for="name" md={2}>
                Name
              </Label>
              <Col md={10}>
                <Input
                  type="text"
                  name="name"
                  id="name"
                  placeholder="enter your name"
                  onChange={this.onType}
                  value={this.state.appData.name}
                />
              </Col>
            </FormGroup>

            <FormGroup row>
              <Label for="age" md={2}>
                Age
              </Label>
              <Col md={10}>
                <Input
                  type="number"
                  name="age"
                  id="age"
                  placeholder="enter your age"
                  onChange={this.onType}
                  value={this.state.appData.age}
                />
              </Col>
            </FormGroup>

            <FormGroup row>
              <Label for="description" md={2}>
                Descriptions
              </Label>
              <Col md={10}>
                <Input
                  type="textarea"
                  name="desc"
                  id="description"
                  placeholder="enter your description"
                  onChange={this.onType}
                  value={this.state.appData.desc}
                />
              </Col>
            </FormGroup>
            <Button
              onClick={this.onSend}
              outline
              color="secondary"
              size="md"
              block
            >
              Submit
            </Button>
          </Form>
          <br />
        </Container>

        <Container md={12} className="text-left">
          {this.state.submit.map((data, idx) => {
            return (
              <div key={idx}>
                <p>Name: {data.name}</p>
                <p>Age: {data.age}</p>
                <p>Description: {data.desc}</p>
                <hr />
              </div>
            );
          })}
        </Container>
      </>
    );
  }
}

export default CrudGilang;
