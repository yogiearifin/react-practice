import React, { useState, useEffect } from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import {
  Container,
  ListGroup,
  ListGroupItem,
  ListGroupItemHeading,
  // ListGroupItemText,
} from "reactstrap";
import "./assets/redux.sass";
// import axios from "axios";
import { getJsonGil } from "../../store/actions/getJsonGil";
import { useSelector, useDispatch } from "react-redux";

const ReduxGil = () => {
  // const [json, setJson] = useState([]);
  const dispatch = useDispatch();
  const jsonData = useSelector((state) => state.getJsonGil.dataJson);

  useEffect(() => {
    // const getData = () => {
    //   axios
    //     .get("https://jsonplaceholder.typicode.com/todos")
    //     .then((res) => setJson(res.data));
    // };
    // getData();
    dispatch(getJsonGil());
  }, []);

  console.log("jsondata", jsonData);
  // console.log(json);
  return (
    <>
      <h1 className="h1-gil text-center">REDUX HOOKS</h1>
      <Container>
      {jsonData?.map((data, idx) => {
        return (
          <ListGroup>
            <ListGroupItem key={idx}>
              <ListGroupItemHeading>{data.title}</ListGroupItemHeading>
              {/* <ListGroupItemText>
                {data.completed} === false ? "false" : "true"
              </ListGroupItemText> */}
            </ListGroupItem>
          </ListGroup>
        );
      })}
      </Container>      
    </>
  );
};

export default ReduxGil;
