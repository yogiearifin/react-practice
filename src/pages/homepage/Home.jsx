import "bootstrap/dist/css/bootstrap.min.css";
import React, { useState } from "react";
import { Container, Button, Modal, Row, Col } from "reactstrap";
import Syohan from "../../components/Syohan/syohan";
import Yogie from "../../components/yogie/yogie";
import Ary from "../../components/Ary/Ary";
import Gilang from "../../components/gilang/gilang";
import imageGilang from "../../components/gilang/assets/logo.png";
// import "../../App.css"
import { BiFemale, BiMale } from "react-icons/bi";

import imageYogie from "../../components/yogie/assets/JoKing.jpg";
import imgSultan from "../../components/sultan/assets/iwae.jpg";
import Sultan from "../../components/sultan/sultan";
import { FaHouseDamage } from "react-icons/fa";
import Panji from "../../components/panji/panji";
import imagePanji from "../../components/panji/assets/gambarr.jpg";
import Denisa from "../../components/denisa/denisa";
import imageDenisa from "../../components/denisa/assets/dnz.jpg";
import imageSugi from "../../components/sugianto/assets/hulk.jpg";
import Sugi from "../../components/sugianto/sugianto";
import imageAry from "../../components/Ary/asset/pepe.jpg";
import Bromo from "../../components/bromo/bromo";
import imageBromo from "../../components/bromo/assets/bromo.jpeg";


import Zuhry from "../../components/Zuhry/zuhry";
import imageZuhry from "../../components/Zuhry/assets/zuhry.jpg";
import Denny from "../../components/denny/denny";
// import imageDenny from "../../denny/assets/kim-young-ji-nm.jpg";


import Iqbal from "../../components/Iqbal/iqbal";
import imageIqbal from "../../components/Iqbal/assets/haha.jpg";


import Jody from "../../components/jody/jody";
//import imageJody from "https://pbs.twimg.com/profile_images/1218305566461714432/o5URcb3o.jpg";

import Saras from "../../components/saras/saras";
import imageSaras from "../../components/saras/assets/sehun.png";

// Meee
import Teddy from "../../components/Teddy/teddy";
import imageTeddy from "../../components/Teddy/assets/teddy.png";

function Home() {
  const [modalSyohan, setModalSyohan] = useState(false);
  const toggleSyohan = () => setModalSyohan(!modalSyohan);
  const [modalTeddy, setModalTeddy] = useState(false);
  const toggleTeddy = () => setModalTeddy(!modalTeddy);
  const [modalYogie, setModalYogie] = useState(false);
  const toggleYogie = () => setModalYogie(!modalYogie);

  const nameTeddy = "Teddy Ferdian Abrar Amrullah";
  const descTeddy = "Teddy Ferdian .A.A is the student of Glints Academy FE #9";

  const [modalSaras, setModalSaras] = useState(false);
  const toggleSaras = () => setModalSaras(!modalSaras);
  const nameSaras = "Dewi Saraswati";
  const descSaras = "Saras 008 Pahlawan Kebajikan";
  const nameYogie = "Yogie Arifin";
  const descYogie = "Yogie Arifin is the industrial trainer of GA FE #9";
  const nameAry = "Ary Tantranesia";
  const descAry = "Ary Tantranesia is the student of GA FE #9";
  const [modalAry, setModalAry] = useState(false);
  const toggleAry = () => setModalAry(!modalAry);

  const nameGilang = "Gilang Adam";
  const descGilang = "Gilang Adam is the student of Glints Academy FE #9";
  const [modalGilang, setModalGilang] = useState(false);
  const toggleGilang = () => setModalGilang(!modalGilang);
  const [modalSugianto, setModalSugianto] = useState(false);
  const toggleSugianto = () => setModalSugianto(!modalSugianto);
  const nameSugi = "Sugianto";
  const descSugi = "Glints Academy Student FE Batch 9";
  const [modalSultan, setModalSultan] = useState(false);
  const toggleSultan = () => setModalSultan(!modalSultan);
  const [modalPanji, setModalPanji] = useState(false);
  const togglePanji = () => setModalPanji(!modalPanji);
  const namePanji = "Panji Bayu";
  const descPanji = "Panji Bayu student of GA FE #9";

  const nameSultan = "Sultan Naufal FP";
  const descSultan = "Sultan Naufal FP is a student of GA FE #9";
  const [modalBromo, setModalBromo] = useState(false);
  const toggleBromo = () => setModalBromo(!modalBromo);
  const nameBromo = "Bromo Yudo Wibowo";
  const descBromo = "Bromo is not KETUA KELAS SIANG of GA FE #9";

  const [modalDenisa, setModalDenisa] = useState(false);
  const toggleDenisa = () => setModalDenisa(!modalDenisa);
  const nameDenisa = "Denisa Pramastuti";
  const descDenisa = "Denisa is home alone tonight";

  const [modalZuhry, setModalZuhry] = useState(false);
  const toggleZuhry = () => setModalZuhry(!modalZuhry);
  const nameZuhry = "Zuhry Abdi Rahmani";
  const descZuhry = "Luwak White Coffe, passwordnya? Kopi nikmat nyaman di lambung.";
  const [modalDenny, setModalDenny] = useState(false);
  const toggleDenny = () => setModalDenny(!modalDenny);
  const nameDenny = "Denny Kurniawan";
  const descDenny = "Programmer and also Progamer :D";


  const [modalIqbal, setModalIqbal] = useState(false);
  const toggleIqbal = () => setModalIqbal(!modalIqbal);
  const nameIqbal = "Muhammad Iqbal";
  const descIqbal = "Hahaha pas kelas gak pernah mandi";

  const [modalJody, setModalJody] = useState(false);
  const toggleJody = () => setModalJody(!modalJody);
  const nameJody = "Muhammad Adnand Jody Pratama";
  const descJody = "Hello world";





  return (
    <>
      <Container className="ga-container">
        <Row>
          <h1>
            GLINTS ACADEMY #9
            <FaHouseDamage />
          </h1>
        </Row>
        <Row>
          <h2>Front End Class</h2>
        </Row>
        <Row>
          <h3>List of members</h3>
        </Row>
        <Row>
          <Col>
            <Row className="male">
              <h4>
                <BiMale />
                Male
              </h4>
            </Row>
            <Row className="ga-row male">
              <Button color="danger" onClick={toggleSyohan}>
                Syohan
              </Button>
              <Modal isOpen={modalSyohan} toggle={toggleSyohan}>
                <Syohan />
              </Modal>
            </Row>
            <Row>
              <Button color="danger" onClick={toggleYogie}>
                Yogie
              </Button>
              <Modal isOpen={modalYogie} toggle={toggleYogie}>
                <Yogie name={nameYogie} desc={descYogie} img={imageYogie} />
              </Modal>
            </Row>
            <Row>
              <Button color="danger" onClick={toggleSultan}>
                Sultan
              </Button>
              <Modal isOpen={modalSultan} toggle={toggleSultan}>
                <Sultan name={nameSultan} desc={descSultan} img={imgSultan} />
              </Modal>
            </Row>
            <Row className="ga-row male">
              <Button color="danger" onClick={toggleGilang}>
                Gilang
              </Button>
              <Modal isOpen={modalGilang} toggle={toggleGilang}>
                <Gilang name={nameGilang} desc={descGilang} image={imageGilang} />
              </Modal>
            </Row>
            <Row>
              <Button color="danger" onClick={toggleBromo}>
                Bromo
              </Button>
              <Modal isOpen={modalBromo} toggle={toggleBromo}>
                <Bromo name={nameBromo} desc={descBromo} img={imageBromo} />
              </Modal>
            </Row>
            <Row className="ga-row male">
              <Button color="danger" onClick={togglePanji}>
                Panji
              </Button>
              <Modal isOpen={modalPanji} toggle={togglePanji}>
                <Panji name={namePanji} desc={descPanji} img={imagePanji} />
              </Modal>
            </Row>
            <Row className="ga-row male">
              <Button color="danger" onClick={toggleSugianto}>
                Sugi
              </Button>
              <Modal isOpen={modalSugianto} toggle={toggleSugianto}>
                <Sugi name={nameSugi} desc={descSugi} img={imageSugi} />
              </Modal>
            </Row>
            <Row className="ga-row male">
              <Button color="primary" onClick={toggleTeddy}>
                Teddy Ferdian Abrar Amrullah
              </Button>
              <Modal isOpen={modalTeddy} toggle={toggleTeddy}>
                <Teddy name={nameTeddy} desc={descTeddy} img={imageTeddy} />
              </Modal>
            </Row>
            <Row className="ga-row male">
              <Button color="dark" onClick={toggleZuhry}>
                Zuhry
              </Button>
              <Modal isOpen={modalZuhry} toggle={toggleZuhry}>
                <Zuhry name={nameZuhry} desc={descZuhry} img={imageZuhry} />
              </Modal>
            </Row>

            <Row className="ga-row male">
              <Button color="dark" onClick={toggleDenny}>
                Denny Kurniawan
              </Button>
              <Modal isOpen={modalDenny} toggle={toggleDenny}>
                <Denny name={nameDenny} desc={descDenny} />
              </Modal>
            </Row>

            <Row className="ga-row male">

              <Button color="dark" onClick={toggleIqbal}>
                Muhammad Iqbal
              </Button>
              <Modal isOpen={modalIqbal} toggle={toggleIqbal}>
                <Iqbal name={nameIqbal} desc={descIqbal} img={imageIqbal} />
              </Modal>
            </Row>

            <Row className="ga-row male">
              <Button color="danger" onClick={toggleJody}>
                Muhammad Adnand Jody Pratama
              </Button>
              <Modal isOpen={modalJody} toggle={toggleJody}>
                <Jody name={nameJody} desc={descJody} img />

              </Modal>
            </Row>
          </Col>
          <Col>
            <Row className="female">
              <h4>
                <BiFemale />
                Female
              </h4>
            </Row>
            <Row className="ga-row female">
              <Button color="danger" onClick={toggleAry}>
                Ary
              </Button>
              <Modal isOpen={modalAry} toggle={toggleAry}>
                <Ary name={nameAry} desc={descAry} img={imageAry} />
              </Modal>
            </Row>
            <Row>
              <Button color="danger" onClick={toggleDenisa}>
                Denisa P.
              </Button>
              <Modal isOpen={modalDenisa} toggle={toggleDenisa}>
                <Denisa name={nameDenisa} desc={descDenisa} img={imageDenisa} />
              </Modal>
            </Row>
            <Row className="ga-row female">
              <Button color="danger" onClick={toggleSaras}>
                Saras
              </Button>
              <Modal isOpen={modalSaras} toggle={toggleSaras}>
                <Saras name={nameSaras} desc={descSaras} img={imageSaras} />
              </Modal>
            </Row>
          </Col>
        </Row>
      </Container>
    </>
  );
};

export default Home;
