import React, { useState, useEffect } from "react";
//import "bootstrap/dist/css/bootstrap.min.css";
import { Button, Row, Col, Container } from "reactstrap";
import { FaSearch } from "react-icons/fa";
//import "./assets/pageYogie.css";
//import image from "../../components/yogie/assets/JoKing.jpg";
//import jadi from "./assets/jadi.png";
import './Jody.css';
import axios from "axios";
import {
  Collapse,
  Navbar,
  NavbarToggler,
  NavbarBrand,
  Nav,
  NavItem,
  NavLink,
  UncontrolledDropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem,
  NavbarText
} from 'reactstrap';

import {
  Card, CardImg, CardText, CardBody,
  CardTitle, CardSubtitle
} from 'reactstrap';

const PageJody = (props) => {

  const [isOpen, setIsOpen] = useState(false);

  const toggle = () => setIsOpen(!isOpen);
  
  const mine = {
    name: "Muhammad Adnand Jody Pratama",
    blurb: "I am a front-end student of Glints Academy Batch 9. I want to be the new member of Samsan Tech.",
    list : ["Japan", "South Korea", "USA"]
  }
  return ( 
    <div className="jody">
      {/* navbar */}
      <div>
          <Navbar color="dark" dark expand="md">
            <NavbarBrand href="/"><h3><span><img class="medium-logo" src="https://cdn.onlinewebfonts.com/svg/img_256332.png"></img></span>edium</h3></NavbarBrand>
            <NavbarToggler onClick={toggle} />
            <Collapse isOpen={isOpen} navbar>
              <Nav className="mr-auto" navbar>
                <NavItem>
                  <NavLink href="#">25 Followers</NavLink>
                </NavItem>
                <NavItem>
                  <NavLink href="#">4 Following</NavLink>
                </NavItem>
                <NavItem>
                  <NavLink href="#">1999 Posts</NavLink>
                </NavItem>
                <UncontrolledDropdown nav inNavbar>
                  <DropdownToggle nav caret>
                    Options
                  </DropdownToggle>
                  <DropdownMenu right>
                    <DropdownItem>
                      Share Profile
                    </DropdownItem>
                    <DropdownItem>
                      Block
                    </DropdownItem>
                    <DropdownItem divider />
                    <DropdownItem>
                      Report
                    </DropdownItem>
                  </DropdownMenu>
                </UncontrolledDropdown>
              </Nav>
              <Button href="/" color="dark">Logout</Button>
            </Collapse>
          </Navbar>
      </div>
      
        <Row>
          <Col sm="3">
            <Card className="kiri">
              <CardImg top width="100%" src="https://drive.google.com/uc?export=view&id=1ghxOFmdOy7ex6wq9VAO3LfAUsd3XmJ1v" alt="Card image cap" />
              <CardBody>
                <CardTitle tag="h5" className="">{mine.name}</CardTitle>
                <CardSubtitle tag="p" className="abu"><strong>Front-end Developer</strong></CardSubtitle>
                <CardText className="desc" tag="p"><p style={{fontSize: "18px"}}>Here you can find some articles related to Web Development, especially for the front-end.</p></CardText>
                <Button>Follow</Button>
              </CardBody>
            </Card>
          <h3></h3>
          </Col>
      
          <Col sm="9">
            <Card className="kanan">
              <CardBody>
                <CardTitle>
                  <p style={{fontSize: "18px"}} className="raleway">Published by Muhammad Adnand Jody Pratama, 6<sup>th</sup> April 2020</p>
                  <h1>Khasiat Tempe untuk Mengatasi Rasa Lapar</h1>
                </CardTitle>
                <CardImg src="https://sgp1.digitaloceanspaces.com/indonesiawindow/2020/10/Tempe.jpg"></CardImg>
                <CardText style={{fontSize: "18px"}}><span className="first-huruf">L</span>orem ipsum dolor sit, amet consectetur adipisicing elit. Reiciendis obcaecati ex aperiam suscipit reprehenderit tenetur repellat voluptate vel, cupiditate velit voluptates sunt, accusamus illo sed ab nobis, consectetur doloremque amet. Lorem ipsum dolor, sit amet consectetur adipisicing elit. A quam molestiae, illum accusamus adipisci sapiente perferendis, quod molestias itaque dolorem sit laudantium facere, vel fugit sint fugiat sequi? Quidem, doloribus!</CardText>
                <CardText style={{fontSize: "18px"}}>Tempora aspernatur quasi accusamus, recusandae aliquid sequi rerum officiis. Odio, vel, laborum eos sequi voluptatem quisquam in sapiente ab quam facilis pariatur! Lorem ipsum dolor sit amet consectetur adipisicing elit. Cum, aspernatur delectus voluptate quas aperiam recusandae sapiente odit facilis eveniet nam quisquam expedita quo dolorem dolor accusamus explicabo neque quia quis.</CardText>
                <CardText style={{fontSize: "18px"}}>Quidem natus accusamus nostrum aut quisquam! Non, blanditiis eos. At quae beatae mollitia explicabo, debitis possimus enim corporis libero! Laborum, a ipsa! Lorem ipsum, dolor sit amet consectetur adipisicing elit. Repellendus deserunt et ex eaque, animi nam nobis reiciendis molestiae sit consequuntur voluptates, error placeat quae veritatis atque sunt reprehenderit, aspernatur dolores!</CardText>
              </CardBody>
            </Card>
          </Col>
        </Row>
      <footer className="text-center footer">Powered by Jody Mantap @2020</footer>
     

      
    </div>
  );
};

export default PageJody;
