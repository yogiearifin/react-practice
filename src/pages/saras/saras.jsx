import "bootstrap/dist/css/bootstrap.min.css";
import React from "react";
import { ModalBody } from "reactstrap";
import "./asset/saras.css";
import { Link } from "react-router-dom";


function Saras(props) {
  const { name, desc, img } = props;
  console.log(props);
  return (
    <>
      <ModalBody>
      <Link to="/saras"><h1>{name}</h1></Link>
        <p>{desc}</p>
        <img src={img} alt="sehun" />
      </ModalBody>
    </>
  );
}

export default Saras;