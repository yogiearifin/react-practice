import React from "react"
import axios from "axios"
import { Container, Row, Col } from "reactstrap"
class JasonSugi extends React.Component {
    state = {
        jasonData: [],
    }

componentDidMount() {
    axios
        .get("https://jsonplaceholder.typicode.com/posts")
        .then((res) => {
            this.setState({
                jasonData: res.data,
            });
        })
        .catch((err) =>{
            console.log(err);
        });
}
render() {
    console.log(this.state.jasonData);
    return (
        <>
            {this.state.jasonData.map((data, idx)=> {
                return (
                    <Container fluid className='content'>
                        <Row>
                            
                            <h2>{data.title}</h2>
                            
                        </Row>
                        <Row>
                            <Col>
                            <p>ID: {data.id}</p>
                            </Col>
                        </Row>   
                        <Row>
                            <Col>
                            <p>{data.body}</p>
                            </Col>
                        </Row>             
                    </Container>
                )
            })}
        </>
    )
}
}
export default JasonSugi