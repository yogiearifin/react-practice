import React from "react";

import Header from "./header";
import Main from "./main";


function PageSugi(){
  return (
    <>
        <Header />
        <Main />
    </>
  );
}

export default PageSugi;