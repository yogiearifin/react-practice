import React, { useState, useEffect } from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import "./assets/Test.css";
import axios from "axios";

const JsonSultan = () => {
  const [states, setStates] = useState([]);
  const getData = () => {
    axios.get("https://jsonplaceholder.typicode.com/posts").then((res) => {
      setStates(res.data);
    });
  };

  useEffect(() => {
    getData();
  }, []);

  console.log(states);

  return (
    <>
      {states.map((data, idx) => {
        return (
          <div className="Parent" key={idx}>
            <div className="Child">
              <h1>Title:{data.title}</h1>
              <p>Body: {data.body}</p>
              <p>ID: {data.id} </p>
            </div>
            <hr />
          </div>
        );
      })}
    </>
  );
};

export default JsonSultan;
