import React from "react";
import "bootstrap/dist/css/bootstrap.min.css";
// import { Button, Row, Col } from "reactstrap";
import { FaSearch } from "react-icons/fa";
import "./assets/PageSultan.css";
import image from "./assets/iwa.jpg";
import img from "./assets/zio.jpg";

import { ModalBody, Container, Col } from 'reactstrap';
import {
  Navbar,
  NavbarBrand,
  Nav,
  NavLink,
  Button, FormGroup, Input,
} from 'reactstrap';
import { faImage } from "@fortawesome/free-solid-svg-icons";
import { Link } from "react-router-dom";

const PageSultan = () => {
  const fontSmall = {
    fontFamily: 'monospace',
    fontWeight: 'bold',
    fontSize: '16px'
  };
  const fontBig = {
    fontFamily: 'monospace',
    fontWeight: 'bold',
    fontSize: '50px'
  };
  const wrapper = {
    display: 'flex',
    marginTop: '100px',
    fontFamily: 'monospace'
  }

  return (
    <div>
      <Navbar color="light" light expand="md">
        <Container>
          <NavbarBrand href="/sultan"><h1 className="text-dark">Sultan</h1></NavbarBrand>
          <Nav className="mr-auto">
            <NavLink href="#"><span className="text-muted">Followers</span></NavLink>
            <NavLink href="#"><span className="text-muted">About</span></NavLink>
            <NavLink href="#"><span className="text-muted">Following</span></NavLink>
            <FormGroup className='m-auto'>
              <Input type="text" name="search" id="search" placeholder="Search" />
            </FormGroup>
            <Button className='searchBTN'><img className='btnIMG' src={FaSearch} alt='' /></Button>
            <Button className='submitBTN'><Link to="/jsonsultan">Goback</Link></Button>
          </Nav>
        </Container>
      </Navbar>
      <Container style={wrapper}>
        <div className='sidebarLeft'>
          <img src={img} alt='alt' />
          <h6>About</h6>
          <h5>Sultan Naufal</h5>
          <p className='fe'>Front-end Developer</p>
          <p style={fontSmall}>Student at Glints X Binar Academy batch 9 FE Class</p>
        </div>
        <div className='sidebarRight'>
          <h6 style={fontSmall}>Published by KAMEN RIDER ZI-O, 4 Desember 2020</h6>
          <h1 style={fontBig}>IWAEEEEE!!!!!!!!!!!!!!!!!!!!</h1>
          <img src={image} alt='alt' />
          <p>Rejoice! The one to inherit all Rider powers, the time king who will rule over the past and the future! Truly, this is the moment Kamen Rider Zi-O is born!</p>
          <p>Zi-O has the greatest power in history. With that power, you can bend not only the world, but also the past and future to your will.</p>
        </div>
      </Container>

    </div>


  );
};


export default PageSultan;
