import React, { useState, useEffect } from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import { Button, Row, Col, Container } from "reactstrap";
// import { FaSearch } from "react-icons/fa";
import "./assets/pageYogie.css";
// import image from "../../components/yogie/assets/JoKing.jpg";
// import jadi from "./assets/jadi.png";
import axios from "axios";
import { Link } from "react-router-dom";
import "./assets/yogie.sass";
import "./assets/yogie.scss";
import { getJson } from "../../store/actions/getJson";
import { useSelector, useDispatch } from "react-redux";
import gambar from "../../components/yogie/assets/JoKing.jpg";

const PageYogie = () => {
  const [json, setJson] = useState([]);
  const dispatch = useDispatch();
  const jsonData = useSelector((state) => state.getJson.dataJson);
  const [num, setNum] = useState(0);
  const [tab, setTab] = useState("");

  const getData = () => {
    axios
      .get("https://jsonplaceholder.typicode.com/todos")
      .then((res) => setJson(res.data));
  };

  const addNum = () => {
    setNum(num + 1);
  };

  const minumNum = () => {
    setNum(num - 1);
  };

  useEffect(() => {
    getData();
    dispatch(getJson());
  }, []);

  const renderTabOpen = () => {
    setTab("open");
  };
  const renderTabClose = () => {
    setTab("close");
  };
  const renderTabReady = () => {
    setTab("ready");
  };

  // useEffect(() => {
  //   console.log("component did mount")
  // }, [])

  // useEffect(() => {
  //   console.log("component did update")
  // }, [num])

  console.log("jsondata", jsonData);
  // console.log(num)
  // console.log(json)

  // const addStates = () => {
  //   setStates([...states, "shutdown pc"]);
  // };

  // // const arr = [
  // //   {
  // //     nama: "saras",
  // //     umur: 26,
  // //     pekerjaan: "student",
  // //     domisili: "bekasi",
  // //   },
  // //   {
  // //     nama: "panji",
  // //     umur: 24,
  // //     pekerjaan: "student",
  // //     domisili: "tulung agung",
  // //   },
  // //   {
  // //     nama: "gary",
  // //     umur: 30,
  // //     pekerjaan: "odong-odong keliling online",
  // //     domisili: "manado",
  // //   },
  // // ];

  return (
    <>
      <Container>
        <div>
          <Button onClick={renderTabOpen}>Open</Button>
          <Button onClick={renderTabClose}>Close</Button>
          <Button onClick={renderTabReady}>Ready</Button>
        </div>
        <div>
          {tab === "overview" ? (
            <div>
              <h1>Open</h1>
            </div>
          ) : tab === "close" ? (
            <div>
              <h1>Close</h1>
            </div>
          ) : tab === "ready" ? (
            <div>
              <h1>Ready</h1>
            </div>
          ) : null}
        </div>
      </Container>
      {/* <Link to="/yogie-json">
        <h1>JSON</h1>
      </Link>
      <Container className="row-center">
        <Row>
          <p>{num}</p>
        </Row>
        <Row>
          <Button onClick={addNum}>+</Button>
          <Button onClick={minumNum}>-</Button>
        </Row>
        {jsonData?.map((data, idx) => {
          return (
            <div key={idx}>
              <p>{data.title}</p>
              <p>{data.body}</p>
              <img src={data.image} />
            </div>
          );
        })}
      </Container> */}
      {/* <div className="sass-div">
        <h1>TEST</h1>
        <h3>H3 SASS</h3>
        <p>INI TEST AJA</p>
        <button className="sass-button">KLIK AKU</button>
      </div>
      <div className="scss-div">
        <h3>SUMPAH INI H3</h3>
        <input type="text"></input>
        <Button className="button-reactstrap">reactstrap</Button>
        <Button className="btns">pure</Button>
      </div>
      <div>
        <img src={gambar} alt="gambars" className="gambar" />
      </div> */}
      {/* {json.map((data, idx) => {
        return (
          <div key={idx}>
            <p>{data.id}</p>
            <p>{data.title}</p>
          </div>
        );
      })} */}
      {/* <Button onClick={() => addStates()}>Shutdown PC</Button>
      {states.map((state, key) => {
        return (
          <div key={key}>
            <p>{state}</p>
          </div>
        );
      })} */}
      {/* <img src={jadi} alt="medium" /> */}
      {/* {arr.map((data, idx) => {
        return (
          <div key={idx} className={idx}>
            <h1>Data siswa</h1>
            <p>{data.nama}</p>
            <p>{data.pekerjaan}</p>
          </div>
        );
      })} */}
    </>
  );
};

export default PageYogie;
