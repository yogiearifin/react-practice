import React from "react";
import {
  Container,
  Col,
  Row,
  Button,
  Form,
  FormGroup,
  Label,
  Input,
} from "reactstrap";
import axios from "axios";

class CrudYogie extends React.Component {
  state = {
    data: {
      name: "",
      age: null,
      desc: "",
    },
    submit: [],
    post: {
      title: "",
      id: null,
      body: "",
    },
  };

  onType = (e) => { //onChange
    console.log(e.target.value);
    this.setState({
      data: {
        ...this.state.data,
        [e.target.id]: e.target.value,
      },
    });
  };

  onInput = (e) => { 
    console.log(e.target.value);
    this.setState({
      post: {
        ...this.state.post,
        [e.target.id]: e.target.value,
      },
    });
  };

  onSend = () => { // onClick
    let allData = [...this.state.submit];
    allData.push({
      name: this.state.data.name,
      age: this.state.data.age,
      desc: this.state.data.desc,
    });
    this.setState({
      submit: allData,
    });
  };

onSub = () => {
  const data = this.state.post
  axios.post("https://jsonplaceholder.typicode.com/posts", { data }).then((res) => {
    console.log(res)
  })
}

  render() {
    console.log("data", this.state.data);
    console.log("submit", this.state.submit);
    // console.log("submit", this.state.post);
    return (
      <>
        <Container className="crud-container">
          <Form>
            <Row form>
              <Col md={6}>
                <FormGroup>
                  <Label for="exampleEmail">Name</Label>
                  <Input
                    type="text"
                    name="name"
                    id="name"
                    placeholder="enter your name"
                    onChange={this.onType}
                  />
                </FormGroup>
              </Col>
              <Col md={6}>
                <FormGroup>
                  <Label for="age">Age</Label>
                  <Input
                    type="number"
                    name="age"
                    id="age"
                    onChange={this.onType}
                  />
                </FormGroup>
              </Col>
            </Row>
            <FormGroup>
              <Label for="desc">Description</Label>
              <Input
                type="text"
                name="desc"
                id="desc"
                placeholder="enter your description"
                onChange={this.onType}
              />
            </FormGroup>
            <Button onClick={this.onSend}>Submit</Button>
          </Form>
          {this.state.submit.map((data, idx) => {
            return (
              <div key={idx}>
                <p>{data.name}</p>
                <p>{data.age}</p>
                <p>{data.desc}</p>
              </div>
            );
          })}
          <h1>POST</h1>
          <Form>
            <Row form>
              <Col md={6}>
                <FormGroup>
                  <Label for="exampleEmail">Title</Label>
                  <Input
                    type="text"
                    name="title"
                    id="title"
                    placeholder="enter your name"
                    onChange={this.onInput}
                  />
                </FormGroup>
              </Col>
              <Col md={6}>
                <FormGroup>
                  <Label for="age">Id</Label>
                  <Input
                    type="number"
                    name="id"
                    id="id"
                    onChange={this.onInput}
                  />
                </FormGroup>
              </Col>
            </Row>
            <FormGroup>
              <Label for="desc">Body</Label>
              <Input
                type="text"
                name="body"
                id="body"
                placeholder="enter your description"
                onChange={this.onInput}
              />
            </FormGroup>
            <Button onClick={this.onSub}>Post</Button>
          </Form>
        </Container>
      </>
    );
  }
}

export default CrudYogie;
