import React, { useState } from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import { FaSearch } from "react-icons/fa";
import "./assets/PageZuhry.css";
import { Link } from "react-router-dom";
import profile from './assets/zuhry.jpg';
import content from './assets/content.jpg';
import {
  Row,
  Col,
  Collapse,
  Navbar,
  NavbarToggler,
  NavbarBrand,
  Nav,
  NavItem,
  NavLink,
  UncontrolledDropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem,
  NavbarText,
  Button, InputGroup, InputGroupAddon, Input
} from 'reactstrap';
import { BiFontFamily } from "react-icons/bi";

const PageZuhry = () => {
  const [isOpen, setIsOpen] = useState(false);
  const toggle = () => setIsOpen(!isOpen);

  // style
  const navbar = {
    height: '100px',
    position: 'fixed',
    top: '0',
    width: '100%',
    zIndex: '999'
  }
  const inputGroup = {
    width: '300px',
    marginRight: '10px'
  };
  const webTitle = {
    fontSize: '36px',
    fontWeight: '400',
    marginLeft: '20px',
    fontFamily: 'Abril Fatface, cursive'
  }

  return (
    <div>
      <Navbar color="light" light expand="md" style={navbar}>
        <NavbarBrand href="#" style={webTitle}>SomeWebsite</NavbarBrand>
        <NavbarToggler onClick={toggle} />
        <Collapse isOpen={isOpen} navbar>
          <Nav className="mr-auto" navbar>
            <NavItem>
              <NavLink href="#">0 Followers</NavLink>
            </NavItem>
            <NavItem>
              <NavLink href="#">About</NavLink>
            </NavItem>
            <NavItem>
              <NavLink href="#">Following</NavLink>
            </NavItem>
            <UncontrolledDropdown nav inNavbar>
              <DropdownToggle nav caret>
                Options
              </DropdownToggle>
              <DropdownMenu right>
                <DropdownItem>
                  Option 1
                </DropdownItem>
                <DropdownItem>
                  Option 2
                </DropdownItem>
                <DropdownItem divider />
                <DropdownItem>
                  Reset
                </DropdownItem>
              </DropdownMenu>
            </UncontrolledDropdown>
          </Nav>
          <InputGroup style={inputGroup}>
            <Input />
            <InputGroupAddon addonType="append">
              <Button color="secondary"><i class="fas fa-search"></i></Button>
            </InputGroupAddon>
          </InputGroup>
          <Button href='/' color="secondary">Go Back</Button>{' '}
        </Collapse>
      </Navbar>
      <div className='zContainer'>
        <div className='zLeftPanel'>
          <img src={profile} alt=''/>
          <h5>About</h5>
          <h4>Zuhry Abdi Rahmani</h4>
          <p className='occ'>Front-end Developer</p>
          <p style={{fontFamily:'Roboto, sans-serif', fontWeight:'300', fontSize:'14px'}}>Lorem ipsum dolor sit amet consectetur, adipisicing elit. Fugit sed nemo cupiditate in et dolore similique ex enim, error laboriosam?</p>
        </div>
        <div className='zRightPanel'>
          <h5 style={{fontFamily:'Roboto, sans-serif', fontWeight:'300', fontSize:'14px'}}>Published by Zuhry Abdi Rahmani, December 4<sup>th</sup> 2020</h5>
          <h1 style={{fontFamily:'Roboto, sans-serif', fontWeight:'700', fontSize:'50px'}}>Bahaya Ketiduran Ketika Berenang</h1>
          <img src={content}/>
          <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Delectus dolores facere doloribus vel minus ipsa optio in, commodi rem iusto asperiores. Veritatis totam quo pariatur atque quasi maiores eius similique! Vitae, odit assumenda debitis quas perferendis voluptas nesciunt nulla explicabo eaque eos. Nemo, voluptas laudantium minus aut ab architecto repellat a quibusdam eum non. Vitae assumenda enim obcaecati voluptas exercitationem necessitatibus autem suscipit iure, nisi non accusantium dolores temporibus harum facere blanditiis ducimus architecto ipsa totam numquam laudantium magni id! Laudantium tempora quidem aliquam quisquam quia velit voluptate nulla eveniet, ipsa nisi, explicabo recusandae et repudiandae cum, error quo architecto.</p>
          <p>Lorem ipsum dolor, sit amet consectetur adipisicing elit. Iste nihil cumque rem ullam sed provident id sit ipsam officiis expedita nisi laboriosam harum, sequi voluptates similique numquam iure dicta adipisci tenetur doloribus unde, voluptas, ratione cum necessitatibus! Rerum blanditiis laborum eaque. Vero sapiente iusto quisquam explicabo sunt molestiae repellendus, ex sint modi voluptates cupiditate non tempora in minus, placeat corporis incidunt distinctio est alias fuga possimus. Ipsum doloremque sed enim, aspernatur reprehenderit hic voluptates corporis recusandae quibusdam molestias doloribus sit expedita laboriosam tempora dolor, nihil eius cum vitae laudantium deserunt. Quam, eaque. Eveniet quos commodi enim excepturi accusantium reiciendis est ad fugit officiis, fuga quod deleniti nihil neque libero eaque provident. Placeat eligendi ipsum vitae qui nemo provident sapiente, autem debitis! Corrupti porro accusamus iure, pariatur asperiores animi blanditiis optio at sunt obcaecati impedit aut provident necessitatibus dicta facilis nesciunt quidem repellat totam, ipsa officiis molestias. Distinctio quod nobis vel alias, culpa deleniti placeat est quo, totam saepe in odio dolor maxime accusamus quam repellendus quidem reprehenderit error? Eius aspernatur maxime pariatur ullam sint nostrum iste laboriosam eaque non laborum porro, suscipit sed fugiat quos sapiente illum a exercitationem repudiandae cum totam iusto ea illo? Officia earum omnis natus eaque.</p>
        </div>
      </div>
    </div>
  );
};

export default PageZuhry;