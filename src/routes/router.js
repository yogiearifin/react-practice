import React from "react";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import Home from "../pages/homepage/Home";
import App from "../App";
import JsonYogie from "../components/yogie/json/jsonYogie";
import JsonDenny from "../components/denny/json/jsonDenny";
import CrudYogie from "../pages/yogie/crudYogie";
import SyohanJson from "../pages/Syohan/SyohanJson";
import PageSyohan from "../pages/Syohan/PageSyohan";
import FormContact from "../pages/Syohan/formcontact";
// import PageSyohan from "../pages/Syohan/PageSyohan";


import Yogie from "../pages/yogie/PageYogie";
import Bromo from "../pages/bromo/PageBromo";
import Zuhry from "../pages/zuhry/PageZuhry";
import Denny from '../pages/denny/PageDenny';
import Denisa from "../pages/denisa/pageDenisa";
import JsonDenisa from "../components/denisa/assets/json/jsonDenisa";
import CrudDenisa from "../pages/denisa/crudDenisa";
import NumbOpt from "../pages/denisa/number";
import Gilang from '../pages/gilang/PageGilang';
import Data from "../pages/gilang/Data";
import CrudGilang from "../pages/gilang/crud.jsx";
import CrudAxiosGilang from "../pages/gilang/crud-axios.jsx";
import CounterGilang from "../pages/gilang/Counter";
import ReduxGil from "../pages/gilang/reduxHooks";
import Sultan from "../pages/sultan/PageSultan";
import JsonSultan from "../pages/sultan/JsonSultan";
import Jody from "../pages/jody/PageJody";

//import Sugianto from '../pages/sugianto/pageSugi';
import JasonSugi from "../pages/sugianto/jsonSugi"

import Saras from "../pages/saras/pageSaras";

// import Sultan from "../pages/sultan/PageSultan";
// import JsonSultan from "../pages/sultan/JsonSultan";
import Sugianto from "../pages/sugianto/pageSugi";
// import Saras from "../pages/saras/pageSaras";

// import Sultan from "../pages/sultan/PageSultan";

// import Sugianto from "../pages/sugianto/pageSugi";
// import Ary from "../pages/ary/PageAry.jsx";

// import Iqbal from '../pages/iqbal/PageIqbal';

// import NotFound from "../pages/404";
// import Ary from "../pages/ary/PageAry.jsx";
// import Iqbal from '../pages/iqbal/PageIqbal';
// import NotFound from "../pages/404";
// import Teddy from '../pages/Teddy/PageTeddy';
// import TeddyJson from '../pages/Teddy/TeddyJson';
// import CrudTeddy from "../pages/Teddy/CrudTeddy";
import SyohanCrud from "../pages/Syohan/SyohanCrud";
import Ary from "../pages/ary/PageAry.jsx";
import Iqbal from "../pages/iqbal/PageIqbal";
import NotFound from "../pages/404";
import Teddy from "../pages/Teddy/PageTeddy";
import TeddyJson from "../pages/Teddy/TeddyJson";
import CrudTeddy from "../pages/Teddy/CrudTeddy";
import HooksTeddy from '../pages/Teddy/HooksTeddy';

// import App from "../App";

// import JsonDenny from "../components/denny/json/jsonDenny";


const Routes = () => {
  return (
    <Router>
      <Switch>
        <Route path="/" exact>
          <Home />
        </Route>
        <Route path="/yogie" exact>
          <Yogie />
        </Route>
        <Route path="/yogie-json" exact>
          <JsonYogie />
        </Route>
        <Route path="/yogie-crud" exact>
          <CrudYogie />
        </Route>
        <Route path="/PageSyohan" exact>
          <PageSyohan />
        </Route>
        <Route path="/SyohanJson" exact>
          <SyohanJson />
        </Route>
        <Route path="/SyohanCrud" exact>
          <SyohanCrud />
        </Route>
        <Route path="/zuhry" exact>
          <Zuhry />
        </Route>
        <Route path="/denny" exact>
          <Denny />
        </Route>
        <Route path="/denisa" exact>
          <Denisa />
        </Route>
        <Route path="/jsonDenisa" exact>
          <JsonDenisa />
        </Route>
        <Route path="/crudDenisa" exact>
          <CrudDenisa />
        </Route>
        <Route path="/number" exact>
          <NumbOpt />
        </Route>
        <Route path="/denny-json" exact>
          <JsonDenny />
        </Route>
        <Route path="/saras" exact>
          <Saras />
        </Route>
        <Route path="/gilang" exact>
          <Gilang />
        </Route>
        <Route path="/data-gilang" exact>
          <Data />
        </Route>
        <Route path="/crud-gilang" exact>
          <CrudGilang />
        </Route>
        <Route path="/crud-axios-gilang" exact>
          <CrudAxiosGilang />
        </Route>
        <Route path="/counter-gil" exact>
          <CounterGilang />
        </Route>
        <Route path="/redux-simple" exact>
          <ReduxGil />
        </Route>
        <Route path="/sultan" exact>
          <Sultan />
        </Route>
        <Route path="/jsonsultan" exact>
          <JsonSultan />
        </Route>
        <Route path="/iqbal" exact>
          <Iqbal />
        </Route>
        <Route path="/sugianto" exact>
          <Sugianto />
        </Route>
        <Route path="/jody" exact>
          <Jody />
        </Route>
        <Route path="/app" exact>
          <App />
        </Route>
        <Route path="/bromo" exact>
          <Bromo />
        </Route>
        <Route path="/jody" exact>
          <Jody />
        </Route>
        <Route path="/ary" exact>
          <Ary />
        </Route>
        
        <Route path="/teddy" exact>
          <Teddy />
        </Route>
        <Route path="/TeddyJson" exact>
          <TeddyJson />
        </Route>
        <Route path="/crudteddy" exact>
          <CrudTeddy />
        </Route>
        <Route path="/hooksteddy" exact>
          <HooksTeddy />
        </Route>

        <Route path="/app" exact>
          <App />
        </Route>
        <Route path="/jasonSugi">
          <JasonSugi />
        </Route>
        <Route path="*">
          <NotFound />
        </Route>
      </Switch>
  </Router>
  );
};

export default Routes;
