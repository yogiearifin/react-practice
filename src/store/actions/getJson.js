import { GET_JSON_BEGIN, GET_JSON_SUCCESS, GET_JSON_FAIL } from "./types";
import axios from "axios";

export const getJson = () => async (dispatch) => {
  dispatch({
    type: GET_JSON_BEGIN,
    loading: true,
    error: null
  });
  try {
    const res = await axios.get("https://jsonplaceholder.typicode.com/posts");
    console.log("res dari action",res)
    dispatch({
      type: GET_JSON_SUCCESS,
      loading: false,
      payload: res.data,
      error:null
    });
  } catch (error) {
    dispatch({
      type: GET_JSON_FAIL,
      error: error.response,
    });
    console.log(error);
  }
};
