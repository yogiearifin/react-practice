import { GET_JSON_START, GET_JSON_SUCCESS, GET_JSON_FAIL } from "./types-gilang";
import axios from "axios";

export const getJsonGil = () => async (dispatch) => {
  dispatch({
    type: GET_JSON_START,
    loading: true,
    error: null
  });
  try {
    const res = await axios.get("https://jsonplaceholder.typicode.com/todos");
    console.log(res)
    dispatch({
      type: GET_JSON_SUCCESS,
      loading: false,
      payload: res.data,
      error:null
    });
  } catch (error) {
    dispatch({
      type: GET_JSON_FAIL,
      error: error.response,
    });
    console.log(error);
  }
};
