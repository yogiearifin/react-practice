import {
  GET_JSON_START,
  GET_JSON_SUCCESS,
  GET_JSON_FAIL,
} from "../actions/types-gilang";

const intialState = {
  dataJson: [],
  loading: false,
  error: null,
};

const getJsonGil = (state = intialState, action) => {
  const { type, payload, error } = action;
  switch (type) {
    default:
      return {
        ...state,
      };
    case GET_JSON_START:
      return {
        ...state,
        loading: true,
      };
    case GET_JSON_SUCCESS:
      return {
        dataJson: payload,
        loading: false,
      };
    case GET_JSON_FAIL:
      return {
        dataJson: [],
        loading: false,
        error: error,
      };
  }
};

export default getJsonGil;
