import { combineReducers } from "redux";
import getJson from "./getJson";
import getJsonGil from "./getJsonGil";

const rootReducers = combineReducers({
  getJson,
  getJsonGil,
});

export default rootReducers;
