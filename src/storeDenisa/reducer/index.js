import { combineReducers } from "redux";
import getJson from "./getJson"

const rootReducers = combineReducers({
    getJson,
});

export default rootReducers;